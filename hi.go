package main

import (
	"fmt"
	"net/http"
)

func main() {
	fmt.Println("Hello. Started on 8090")
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hi %s\n", r.URL.Path)
	})
	http.ListenAndServe(":8090", nil)
}
